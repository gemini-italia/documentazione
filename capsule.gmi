# Capsule Gemini in italiano

Questo è un elenco in ordine alfabetico di capsule in lingua italiana (anche in più lingue tra cui l'italiano):

=> gemini://andreafeletto.com/ gemini://andreafeletto.com/
> Ciao, sono Andrea e attualmente studio Ingegneria Meccatronica presso l'Università degli Studi di Trento.

=> gemini://birabittoh.smol.pub/it gemini://birabittoh.smol.pub/it
> […]programmo dal 2016 e la considero la mia passione. Apprezzo l'arte in ogni sua forma, inclusi i videogiochi, così come ogni tipo di humor. Mi piace passare il tempo su Discord programmando roba che probabilmente non finirò mai.

=> gemini://cobradile.cities.yesterweb.org/index.it.gmi gemini://cobradile.cities.yesterweb.org/index.it.gmi
>Ciao, io sono "Cobra!", da Glasgow, ma abito in North Lanarkshire adesso. Imparare lingue è una mia ossessione, […] e le voglio imparare tutte! Creo anche dei giochi e film.

=> gemini://cybervalley.org/ gemini://cybervalley.org/
> Questo è un piccolo sito gemini più di prova che altro in cui metterò un minimo di contenuti personali e non.

=> gemini://dariolob.smol.pub/it gemini://dariolob.smol.pub/it
> Average IT guy from Italy. Passionate about retrocomputing

=> gemini://firenze.linux.it/ gemini://firenze.linux.it/
> Questo è il sito gemini del Firenze Linux User Group o FLUG, un insieme di persone che promuovono e supportano la diffusione del software libero e del sistema operativo GNU/Linux sul territorio fiorentino.

=> gemini://gemini.abiscuola.com/ gemini://gemini.abiscuola.com/
> Furnace of software that (almost) work.

=> gemini://gemini.iosa.it/ gemini://gemini.iosa.it/
> La capsula Gemini di steko. Penso che questa capsula sia la prima scritta in italiano nel mondo Gemini, in orbita dal 28 dicembre 2020. Ma l'importante non è essere i primi ad arrivare, ma essere sicuri di essere partiti. In viaggio, e in compagnia!

=> gemini://gemlog.blue/users/roberto_vpt/ gemini://gemlog.blue/users/roberto_vpt/
Il gemlog di roberto_vpt.

=> gemini://it.omarpolo.com/ gemini://it.omarpolo.com/

=> gemini://omg.pebcak.club/~cage/ gemini://omg.pebcak.club/~cage/
La capula di cage contiene una grande quantità di opinioni non richieste.

=> gemini://omg.pebcak.club/~freezr/ gemini://omg.pebcak.club/~freezr/
> I like writing about computing PEBCAK experiences, tecno-ethical delirium and multi-disciplinary art experiments!

=> gemini://tilde.team/~mario/ gemini://tilde.team/~mario/
> This geminispace is run by Mario Sabatino from Rome, Italy.

=> gemini://woodpeckersnest.space/ gemini://woodpeckersnest.space/
> @wpn is a no-profit project […]: it offers some services to friends and acquaintances, like an XMPP/Email address or a fully featured shell account with access to pretty much everything that is being shared/hosted by the server.

Se volete essere aggiunti chiedete sul canale IRC (#gemini-it sul server libera.chat) o proponete modifiche al repository della documentazione.

## Capsule non più attive

=> gemini://alsd.eu/it/                  gemini://alsd.eu/it/
=> gemini://fmpoerio.eu/                 gemini://fmpoerio.eu/
=> gemini://gemini-italia.europecom.net/ gemini://gemini-italia.europecom.net/
=> gemini://gemini.barca.mi.it/          gemini://gemini.barca.mi.it/
=> gemini://head.baselab.org/            gemini://head.baselab.org/
=> gemini://heathens.club/~memo/         gemini://heathens.club/~memo/
=> gemini://koyu.space/octt/             gemini://koyu.space/octt/
=> gemini://koyu.space/zuz/              gemini://koyu.space/zuz/
=> gemini://nicfab.eu/                   gemini://nicfab.eu/
=> gemini://tommi.space/it/index.gmi     gemini://tommi.space/it/index.gmi
=> gemini://tracciabi.li/whiterabbit/    gemini://tracciabi.li/whiterabbit/
=> gemini://vidage.rocks/                gemini://vidage.rocks/

Ultimo aggiornamento 2024-12-23
