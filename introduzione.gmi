# Introduzione a gemini

Con questo documento vorremmo introdurre in termini semplici ma, per quanto ci è possibile, corretti una presentazione delle motivazioni che hanno contribuito alla costituzione della rete gemini, in tal modo speriamo che sia possibile, per i nuovi utenti, un utilizzo consapevole di questo strumento di comunicazione.

Non possiamo, in questo articolo, fornire una descrizione completa delle specifiche formali[1], ma piuttosto una panoramica dei principi che ne hanno ispirato l'ideazione e alcune risorse (se possibile in italiano) per iniziare a esplorare i contenuti che sono già disponibili.

## Definizione

Gemini altro non è che un insieme di documenti che definiscono:

* un linguaggio comune attraverso il quale due programmi per computer possono comunicare tra loro (in termini più strettamente tecnici: un protocollo di rete di livello 7, o "livello dell'applicazione") [2];

* un formato di file per ipertesti, documenti che possono fare rifermanti ad altri file.

Le specifiche relative al protocollo definiscono un meccanismo di comunicazione tra computer che permette ad un programma (client [3]) di ricevere dati da un altro computer che fornisce un punto di accesso (il server [4]).

In questo modo è possibile, per il server, mettere a disposizione file di ipertesto perché possano essere consultati dagli utenti tramite l'uso di un client. Un collegamento all'interno di un documento ipertestuale può portare ad un documento presente anche su un altro server creando cosi una una "ragnatela" di scritti [5].

Il protocollo non è limitato alla consultazione di ipertesti, qualunque tipo di file può essere trasmesso attraverso questo canale di comunicazione, è possibile anche una trasmissione di dati tra client e server che renda il primo più che un mero strumento passivo di ricezione di informazioni, infatti sono già disponibili applicazioni che permettono di chiacchierare oppure di prendersi cura di un proprio alberello virtuale [6,7]!

## I principi

### Un approccio differente al web

Quanto scritto nel paragrafo precedente ricorderà a molte persone quella che è, probabilmente, l'accoppiata più famosa della rete internet: il protocollo per ipertesti HTTP [8] e il formato di file HTML [9].

Tali persone non si sbagliano di molto! In effetti, pur senza mirare a volerlo sostituire, le funzionalità di gemini sono parzialmente sovrapponibili a quelle degli strumenti summenzionati, tuttavia ciò che cambia profondamente è l'approccio al problema della distribuzione delle informazioni.

Negli anni quello che viene chiamato in gergo World Wide Web (WWW) è diventato uno strumento sempre più complesso, tanto complesso da impedire, di fatto, l'ingresso di attori nel panorama dei browser che non siano costituiti da aziende che dispongono di grandi capitali. Le stesse aziende che indirizzano (o provano a indirizzare) lo sviluppo degli standard di rete verso i propri interessi, che non sempre coincidono con quelli pubblici. Valga come semplice esempio i meccanismi che impediscono da parte degli utenti di disporre come meglio credono dei contenuti scaricati (DRM [10,11])

Inoltre l'utilizzo di strumenti che non sono strettamente necessari per veicolare le informazioni (ad esempio: immagini pubblicitarie o script che trasformano ogni pagina in un vero e proprio programma eseguito nel client dell'utente) costituiscono un (neanche tanto) potenziale problema sia per la riservatezza dell'utente che per la sicurezza del proprio computer. Ancora, l'enorme massa di tali dati non strettamente necessari nasconde in se un problema ecologico di non poco conto: ogni bit che scambiamo sulla rete ha un costo in termini energetici e quindi di risorse ambientali [12].

A partire da queste premesse l'approccio di gemini è stato progettare un sistema di comunicazione che fosse:

* rispettoso della riservatezza degli utenti;

  Gemini è progettato cercando di essere il meno estensibile possibile; ciò significa che non è possibile inserire informazioni con strutture arbitrarie che non siano state formalmente inquadrate all'interno del protocollo. Ad esempio non si può chiedere ad un client di immagazzinare e restituire successivamente dati arbitrariamente forniti dal server, cosa che si può, invece, fare usando il meccanismo dei cookie [13]

  Il formato gemini non permette che siano aperte connessioni di rete senza una specifica azione da parte dell'utente [14].

* parco di risorse

  La struttura del protocollo e il fatto che un file gemini si concentri sui contenuti e non su come tali contenuti vadano presentati, permette di scrivere programmi semplici e che usino poche risorse energetiche senza che questo pregiudichi la fruizione delle informazioni.

* semplice da implementare;

  Gemini è un protocollo sufficientemente semplice da permettere lo sviluppo di client o server da parte di molte entità, che siano programmatori dilettanti od aziende quotate in borsa. Ciò dovrebbe anche far desistere attori da tattiche che tendono al monopolio o all'indirizzamento unilaterale delle specifiche del protocollo.

  Il formato di file gemini, d'altronde, è un semplice file di testo leggibile anche senza che sia interpretato e trasformato da un client [15].

  Tali caratteristiche permettono, inoltre, più persone di gestire un proprio server invece di affidarsi ad altri per ospitare i propri contenuti, anche questo può migliorare sia la diversità di opinioni e punti di vista con meno timore di censure e costruire una rete più a misura di essere umano.

### Semplice ma non semplicistico

Il termine "semplice" potrebbe trarre in inganno: gemini è semplice ma non per questo va considerato come un protocollo banale o inutile per un utilizzo diverso dal puro diletto.

Nella definizione del protocollo si è badato a che la facilità di implementazione dei protocolli non andasse a scapito della riservatezza dell'utente.

Per questo motivo, ad esempio, gemini prevede obbligatoriamente la cifratura delle connessioni tra client e server utilizzando il sistema TLS [16], lo stesso sistema utilizzato per le transazioni "sicure" sul WWW.

## Gemini in pratica

### Accedere ai contenuti

   Per accedere alla rete gemini avete bisogno di un programma che vi permetta di collegarvi ad un server, ovvero un client.

   Per fortuna vi sono disponibili molti client con caratteristiche molto diverse, alcuni usano un'interfaccia testuale, altri sono più simili ai browser più comuni; alcuni possono anche funzionare su dispositivi mobili:

   Una lista -senz'altro non esaustiva-  è recuperabile qui:

=> https://gemini.circumlunar.space/clients.html Una lista di client gemini

=> gemini://gemini.circumlunar.space/software/ Una lista di client più completa (contiene anche una lista di server che implementano il protocollo)

Questi client hanno sviluppatori che parlano in italiano:

=> https://telescope.omarpolo.com/
=> https://www.autistici.org/interzona/tinmop.html

Se non vi è la possibilità di installare uno di questi programmi sono disponibili anche dei siti internet che fanno da intermediari tra gemini e l'HTTP detti proxy come quello disponibile a questo indirizzo:

=> https://portal.mozz.us/ Proxy tra gemini e HTTP

## Distribuire i contenuti

Nel caso decideste di ospitare dei vostri documenti su un vostro server (detto "capsula"), un vostro diario (in gergo "gemlog") o altri file sulla rete gemini avrete bisogno di un server; anche in questo caso vi è ampia possibilità di scegliere tra implementazioni in vari tipi di linguaggi e differenti livelli di complessità in termini di configurazione e messa in opera:

=> gemini://gemini.circumlunar.space/software/ Una lista di client e server

Questi server hanno sviluppatori che parlano in italiano:

=> https://gmid.omarpolo.com/

# Aiuto

Tutti ne abbiamo bisogno, prima o poi. :) Se il problema riguarda gemini forse possiamo aiutarvi, per il momento potete:

* consultare il resto della documentazione che avete trovato insieme a questo articolo

=> https://codeberg.org/gemini-italia/documentazione

* Connettetervi a IRC[17], server irc.libera.chat canale #gemini-it

# Risorse in italiano sulla rete gemini

## Gemlog

I gemlog sono l'equivalente Gemini dei blog.

=> capsule.gmi Un elenco di capsule e gemlog in lingua italiana

# Note

=> https://gemini.circumlunar.space/docs/specification.html [1] Specifiche gemini
=> https://it.wikipedia.org/wiki/Livello_di_applicazione [2] Wikipedia: Livello di applicazione
=> https://it.wikipedia.org/wiki/Client [3] Wikipedia: Client
=> https://it.wikipedia.org/wiki/Server [4] Wikipedia: Server
=> https://it.wikipedia.org/wiki/Ipertesto [5] Wikipedia: Ipertesto
=> gemini://astrobotany.mozz.us/ [6] Giardino virtuale
=> gemini://chat.mozz.us/ [7] Chat con gemini
[8] Si può pensare che il protocollo implementi qualcosa di molto simile al solo verbo GET dell'HTTP 1.0

=> https://it.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Riga_di_richiesta

=> https://it.wikipedia.org/wiki/HTML [9] Wikipedia: Il formato HTML
=> https://it.wikipedia.org/wiki/Digital_rights_management [10] Wikipedia: DRM
=> https://en.wikipedia.org/wiki/Encrypted_Media_Extensions [11] Wikipedia [EN] DRM dei browser
=> https://www.bloomberg.com/news/features/2020-04-01/how-much-water-do-google-data-centers-use-billions-of-gallons [12] NYT: Uso di acqua per raffreddamento nei datacenter
[13] o del sistema di "local storage" fornito dai browser
[14] quindi niente richieste di pixel traccianti "sottobanco"
[15] infatti è il formato del testo che state leggendo in questo momento!
=> https://it.wikipedia.org/wiki/Transport_Layer_Security [16] Wikipedia: TLS
=> https://it.wikipedia.org/wiki/Internet_Relay_Chat [17] Wikipedia: IRC

© 2021 cage rilasciato con licenza CC-BY-SA

=> https://creativecommons.org/licenses/by-sa/4.0/deed.it
